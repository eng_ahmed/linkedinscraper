#!/usr/bin/env python2
# Description: LinkedIn employee enumeration script.
# Developer(s): Nick Sanzotta and Jacob Robles.
"""linkScrape Core."""

import json
import os
import sys
import time
try:
    import requests
    from bs4 import BeautifulSoup
    from linkArguments import *
    from linkMangle import *
except Exception as errmessage:
    print(("\n[!] Error: %s" % (errmessage)))
    sys.exit(1)

# Authentication URLs
url_base = 'https://www.linkedin.com'
url_login = 'https://www.linkedin.com/uas/login-submit'
# Company search URL.
url_company = 'https://www.linkedin.com/ta/federator?orig=GLHD&verticalSelector=all&query='

# Mangle timestamp.
Mangle_timestamp = time.strftime("%m-%d-%Y_%H:%M")


class LinkScraper:
    """LinkedIn Scraper class."""

    def __init__(self, url_company, username, password):
        """..."""
        self.timestamp = time.strftime("%m-%d-%Y_%H:%M")
        self.username = username
        self.password = password
        self.url_company = url_company
        self.session = self.authenticate(url_base, url_login)
        self.log_path = 'logs/'

        if not os.path.exists(self.log_path):
            os.mkdir(self.log_path)

    def authenticate(self, url_base, url_login):
        """Create a persistent HTTP session to LinkedIn.com."""
        # LinkedIn's Authentication errors.
        auth_error_password = "Hmm, that's not the right password."
        auth_error_generic = r"We\'re sorry. Something unexpected happened and your request could not be completed. Please try again."
        # HTTP client with persistent cookies..
        session = requests.Session()
        # Set user agent.
        user_agent = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.11)'}
        session.headers.update(user_agent)
        # HTTP POST parameters
        html = session.get(url_base).content
        soup = BeautifulSoup(html, 'lxml')
        csrf = soup.find(id="loginCsrfParam-login")['value']
        http_params = {
            'session_key': self.username,
            'session_password': self.password,
            'loginCsrfParam': csrf,
        }
        # Authenticate with persistent cookies.
        try:
            response = session.post(url_login, data=http_params)
            print(('[linkScrape] Request: %s' % (url_login)))
            if response.status_code == 200:
                print(('[LinkedIn.com] Response: HTTP/1.1 %s OK' % (response.status_code)))
            else:
                print(('[LinkedIn.com] Response: HTTP/1.1 %s' % (response.status_code)))
            # Password error.
            if auth_error_password in response.text:
                print("[LinkedIn.com] Hmm, that's not the right password.")
                sys.exit(2)
            # Generic error, most likely invalid username.
            elif auth_error_generic in response.text:
                print("[LinkedIn.com] We\'re sorry. Something unexpected happened and your request could not be completed. Please try again.")
                print("[linkScrape] likely an invalid username caused this error.")
                sys.exit(2)
        except Exception as errmessage:
            print(('[!] Error: %s' % (errmessage)))

        return session

    def company_search(self, company_name):
        """Perform a company search on LinkedIn."""
        # Use the persistent HTTP session to LinkedIn.com
        session = self.session
        try:
            response = session.get(self.url_company + company_name)
            print(('[linkScrape] Request: %s' % (response.url)))
            if response.status_code == 200:
                print(('[LinkedIn.com] Response: HTTP/1.1 %s OK' % (response.status_code)))
            else:
                print(('[LinkedIn.com] Response: HTTP/1.1 %s' % (response.status_code)))
            json_data = json.loads(response.content)
            print (json_data)
        except ValueError as errmessage:
            print(('[linkScrape] Error: %s' % (errmessage)))
            print('[linkScrape] This is a common error, for best results try searching with a LinkedIn Company ID.')
            print('[linkScrape] Exiting ...')
            sys.exit(2)

        # Display the number of items in the JSON list "resultList"
        num_results = len(json_data['resultList'])
        print ("num_results:", num_results)
        # Build a list for searched Companies
        company_list = []
        company_info = []
        for index in range(num_results):
            print("index:",index)
            if 'company' in json_data['resultList'][index]['sourceID']:
                company_list.append(json_data['resultList'][index]['displayName'])
                company_info.append([json_data['resultList'][index]['displayName'],
                                    json_data['resultList'][index]['subLine'],
                                    json_data['resultList'][index]['id'],
                                    json_data['resultList'][index]['url']])
            elif 'mynetwork' in json_data['resultList'][index]['sourceID']:
                company_list.append(json_data['resultList'][index]['displayName'])
                company_info.append([json_data['resultList'][index]['displayName'],
                                    json_data['resultList'][index]['subLine'],
                                    json_data['resultList'][index]['id'],
                                    json_data['resultList'][index]['url'],
                                    json_data['resultList'][index]['misc']])
        print ("Company_info:", company_info)
        print ("company_list:", company_list)

        return company_list, company_info

    



def cls():
    """Clear screen."""
    os.system('cls' if os.name == 'nt' else 'clear')






def name(company_name, output, format_value, domain):
    """Managle function."""
    # Check if directory exists.
    directory_path = '%s%s' % ('logs/', company_name)
    if not os.path.exists(directory_path):
        os.mkdir(directory_path)
    # Create filename.
    filename = '%s%s_%s' % ('mangle', str(format_value), Mangle_timestamp)
    file_path = os.path.join(directory_path, filename)
    # Check if input file exists.
    if not os.path.isfile(output):
        print("[linkMangle] Error: name file not found.")
        print("[linkMangle] Exiting...")
        sys.exit(2)

    names = []
    # Generate a list of first and last name, then call mangle
    for line in open(output, 'r'):
        full_name = ''.join([c for c in line if c == " " or c.isalpha()])
        full_name = full_name.lower().split()
        names.append([full_name[0], full_name[-1]])

    if format_value == 1:
        newname = mangleOne(names, company_name, domain, file_path)
    elif format_value == 2:
        newname = mangleTwo(names, company_name, domain, file_path)
    elif format_value == 3:
        newname = mangleThree(names, company_name, domain, file_path)
    elif format_value == 4:
        newname = mangleFour(names, company_name, domain, file_path)
    elif format_value == 5:
        newname = mangleFive(names, company_name, domain, file_path)
    elif format_value == 6:
        newname = mangleSix(names, company_name, domain, file_path)
    elif format_value == 7:
        newname = mangleSeven(names, company_name, domain, file_path)
    elif format_value == 8:
        newname = mangleEight(names, company_name, domain, file_path)
    elif format_value == 9:
        newname = mangleNine(names, company_name, domain, file_path)
    elif format_value == 10:
        newname = mangleTen(names, company_name, domain, file_path)
    elif format_value == 11:
        newname = mangleEleven(names, company_name, domain, file_path)
    elif format_value == 12:
        newname = mangleTwelve(names, company_name, domain, file_path)
    elif format_value == 13:
        newname = mangleThirteen(names, company_name, domain, file_path)
    elif format_value == 14:
        newname = mangleFourteen(names, company_name, domain, file_path)
    elif format_value == 15:
        newname = mangleFifteen(names, company_name, domain, file_path)
    elif format_value == 16:
        newname = mangleSixteen(names, company_name, domain, file_path)
    elif format_value == 99:
        mangleAll(names, company_name, domain, file_path)
    else:
        sys.exit(2)

    print(('[linkMangle] Created mangle file: %s' % (file_path)))


if __name__ == "__main__":
    '''Main Menu.'''
    cls()
    print(banner)
    # Argparse vars
    args = parse_args()
	#print (args)
    company_name = args.company
    page_range = args.range
    university = args.university
    # Code from version 1.x
    if args.input:
        print((args.input))
        name(args.company, args.input, args.mangle, args.domain)
        sys.exit(0)
    # ClassInstance
    linkScrape = LinkScraper(url_company, args.email, args.password)

    if company_name.isdigit():
        company_selected = True
        cid = company_name
        print(('[linkScrape] Detected LinkedIn Company ID: %s' % (cid)))
    else:
        company_selected = False
        # Company search - while loop, outer
        company_choice = 0
        while not company_selected:
            company_list, company_profile = linkScrape.company_search(company_name)
            print("company_profile:", company_profile)
            
            # Index selection - while loop, inner
            index_select = False
            num_companies = len(company_list)
            print("num_companies:" , num_companies)
            while not index_select:
                print(('\n %s: %s' % (str(0), 'Search again')))
                for index in range(num_companies):
                    print((' %s; %s; %s; %s; %s; %s' % (str(index + 1), company_list[index], company_profile[index][1], company_profile[index][2], company_profile[index][3],company_profile[index][4])))
                #Get input, if not num, prompt again
                try:
                    company_choice = int(eval(input('\n[linkScrape] Please select a menu item: ')))
                except ValueError:
                    continue
                